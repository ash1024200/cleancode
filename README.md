# README #

Tax Calculator Application - This application accepts a list of product details and calculates applicable taxes which are then displayed as a receipt.

### How do I get set up? ###

Prerequisites:
Gradle should be installed on the system where this project is to be run.

Steps:

1. clone this repository
2. enter the cleancode folder
3. run gradle build
4. enter the build/libs folder
5. run the following command to run the application:
```
#!bash

java -cp cleancode.jar com.cleancode.TaxCalculatorConsoleApplication

```