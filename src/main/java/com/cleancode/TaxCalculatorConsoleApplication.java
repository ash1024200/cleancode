package com.cleancode;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.cleancode.inventory.Inventory;
import com.cleancode.receipt.Receipt;
import com.cleancode.receipt.ReceiptCalculator;

/**
 * This class creates sales receipts for products. It reads input from the
 * console and writes output to the console.
 * 
 * @author ashwinvinod
 *
 */
public class TaxCalculatorConsoleApplication {

	private static final String INPUT_CHARSET = "UTF-8";
	private static final String END_INPUT = "";

	public static void main(String[] args) throws Exception {
		TaxCalculatorConsoleApplication taxCalculatorApplication = new TaxCalculatorConsoleApplication();
		taxCalculatorApplication.runApplication();
	}

	private void runApplication() {
		List<String> productDetails = readProductDescriptionsFromConsole();

		Inventory inventory = new Inventory(productDetails);
		ReceiptCalculator receiptBuilder = new ReceiptCalculator(inventory);
		Receipt receipt = receiptBuilder.buildReceipt();

		printReceiptToConsole(receipt);
	}

	private List<String> readProductDescriptionsFromConsole() {
		List<String> inputStrings = new ArrayList<String>();

		System.out.println("Enter purchased items. Press enter when done.");
		Scanner s = new Scanner(System.in, INPUT_CHARSET);
		while (true) {
			String input = s.nextLine();
			if (END_INPUT.equals(input)) {
				break;
			}
			inputStrings.add(input);
		}
		s.close();
		return inputStrings;
	}

	private void printReceiptToConsole(Receipt receipt) {
		System.out.println("Output: ");
		for (String detailsOfProduct : receipt.getProductDetails()) {
			System.out.println(detailsOfProduct);
		}
		System.out.printf("Sales Taxes: %s", receipt.getTotalSalesTax());
		System.out.printf("%nTotal: %s%n", receipt.getTotalAmount());
	}
}
