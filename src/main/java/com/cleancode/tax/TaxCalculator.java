package com.cleancode.tax;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.cleancode.configuration.CONFIG_PROPERTIES;
import com.cleancode.configuration.PropertiesHelper;
import com.cleancode.inventory.InventoryItem;
import com.cleancode.inventory.Product;

/**
 * Given a list of products, this class is responsible for computing applicable
 * taxes.
 * 
 * @author ashwinvinod
 *
 */
public class TaxCalculator {

	private static final String LIST_DELIMITER = ",";

	public double computeTotalTax(InventoryItem inventoryItem) {
		Product product = inventoryItem.getProduct();

		double importTax = determineImportTax(product);
		double salesTax = determineSalesTax(product);
		double totalTax = computeTotalTax(inventoryItem, importTax, salesTax);

		return totalTax;
	}

	private double determineImportTax(Product product) {
		double importTax = 0.0;
		double preTaxCost = product.getPreTaxCost();
		boolean isImportTaxApplicable = determineIfImportTaxIsApplicable(product
				.getUnparsedProductDetails());
		if (isImportTaxApplicable) {
			String importTaxRate = PropertiesHelper
					.getProperty(CONFIG_PROPERTIES.IMPORT_TAX.getPropertyName());
			importTax = (Double.valueOf(importTaxRate) * preTaxCost);
		}
		return importTax;
	}

	private boolean determineIfImportTaxIsApplicable(String input) {
		boolean isImportTaxApplicable = input.toLowerCase(Locale.ENGLISH)
				.contains("import");
		return isImportTaxApplicable;
	}

	private double determineSalesTax(Product product) {
		double salesTax = 0.0;
		double preTaxCost = product.getPreTaxCost();
		boolean isSalesTaxApplicable = determineIfSalesTaxIsApplicable(product
				.getUnparsedProductDetails());
		if (isSalesTaxApplicable) {
			String salesTaxRate = PropertiesHelper
					.getProperty(CONFIG_PROPERTIES.BASIC_SALES_TAX
							.getPropertyName());
			salesTax = (Double.valueOf(salesTaxRate) * preTaxCost);
		}
		return salesTax;
	}

	private boolean determineIfSalesTaxIsApplicable(String description) {
		boolean isSalesTaxApplicable = true;
		if (description == null) {
			return false;
		}

		String taxExemptList = PropertiesHelper
				.getProperty(CONFIG_PROPERTIES.SALES_TAX_EXEMPT
						.getPropertyName());
		List<String> taxExemptItems = Arrays.asList(taxExemptList
				.split(LIST_DELIMITER));
		String descriptionLowerCase = description.toLowerCase(Locale.ENGLISH);

		for (String taxExemptItem : taxExemptItems) {
			if (descriptionLowerCase.contains(taxExemptItem)) {
				isSalesTaxApplicable = false;
				break;
			}
		}
		return isSalesTaxApplicable;
	}

	private double computeTotalTax(InventoryItem inventoryItem,
			double importTax, double salesTax) {
		double totalTax = 0.0;
		int quantity = inventoryItem.getQuantity();
		totalTax = quantity * (importTax + salesTax);

		double totalTaxRoundedUp = Math.ceil(totalTax * 20.0) / 20.0;
		return totalTaxRoundedUp;
	}
}
