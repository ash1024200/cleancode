package com.cleancode.receipt;

import java.util.List;

import com.cleancode.configuration.CONFIG_PROPERTIES;
import com.cleancode.configuration.PropertiesHelper;

public class Receipt {

	private List<String> productDetails;
	private String totalSalesTax;
	private String totalAmount;

	public Receipt(List<String> productDetails, double totalSalesTax,
			double totalAmount) {
		super();

		String decimalFormatting = PropertiesHelper
				.getProperty(CONFIG_PROPERTIES.DECIMAL_FORMATTING
						.getPropertyName());
		this.productDetails = productDetails;
		this.totalSalesTax = String.format(decimalFormatting, totalSalesTax);
		this.totalAmount = String.format(decimalFormatting, totalAmount);
	}

	public String getTotalSalesTax() {
		return totalSalesTax;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public List<String> getProductDetails() {
		return productDetails;
	}
}
