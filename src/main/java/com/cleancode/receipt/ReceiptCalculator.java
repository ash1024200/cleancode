package com.cleancode.receipt;

import java.util.ArrayList;
import java.util.List;

import com.cleancode.configuration.CONFIG_PROPERTIES;
import com.cleancode.configuration.PropertiesHelper;
import com.cleancode.inventory.Inventory;
import com.cleancode.inventory.InventoryItem;
import com.cleancode.inventory.Product;
import com.cleancode.tax.TaxCalculator;

/**
 * This class is responsible for calculating the total cost of a list of
 * products and generating a receipt.
 * 
 * @author ashwinvinod
 *
 */
public class ReceiptCalculator {
	private Inventory inventory;
	private TaxCalculator taxCalculator;

	public ReceiptCalculator(Inventory inventory) {
		super();
		this.inventory = inventory;
		taxCalculator = new TaxCalculator();
	}

	public Receipt buildReceipt() {
		List<String> productDetails = new ArrayList<String>();
		double totalSalesTax = 0.0;
		double totalAmount = 0.0;
		for (InventoryItem inventoryItem : inventory.getInventoryItems()) {

			double totalTaxRate = taxCalculator.computeTotalTax(inventoryItem);
			Product product = inventoryItem.getProduct();
			double productCostIncludingTax = (inventoryItem.getQuantity() * product
					.getPreTaxCost()) + totalTaxRate;
			productDetails.add(getDetailsForProductWithTotalCost(product,
					productCostIncludingTax));

			totalSalesTax += totalTaxRate;
			totalAmount += productCostIncludingTax;
		}

		return new Receipt(productDetails, totalSalesTax, totalAmount);
	}

	private String getDetailsForProductWithTotalCost(Product product,
			double productCostWithTax) {
		String finalProductDetails = "";

		String productDetails = product.getUnparsedProductDetails().split(
				" at ")[0]
				+ ": ";

		String decimalFormatting = PropertiesHelper
				.getProperty(CONFIG_PROPERTIES.DECIMAL_FORMATTING
						.getPropertyName());
		String productCost = String.format(decimalFormatting,
				productCostWithTax);

		finalProductDetails = productDetails + productCost;

		return finalProductDetails;
	}
}
