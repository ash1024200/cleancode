package com.cleancode.configuration;

/**
 * Represents configuration property keys that are contained in a properties
 * file.
 * 
 * @author ashwinvinod
 *
 */
public enum CONFIG_PROPERTIES {
	SALES_TAX_EXEMPT("salesTaxExemptList"), IMPORT_TAX("importTax"), BASIC_SALES_TAX(
			"basicSalesTax"), DECIMAL_FORMATTING("decimalFormatting");

	private String propertyName;

	private CONFIG_PROPERTIES(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyName() {
		return this.propertyName;
	}
}