package com.cleancode.configuration;

import java.io.IOException;
import java.util.Properties;

public class PropertiesHelper {

	private static Properties properties;

	static {
		loadProperties();
	}

	/**
	 * Retrieves a property value from the configuration file for the given key
	 * 
	 * @param key
	 * @return
	 */
	public static String getProperty(String key) {
		return properties.getProperty(key);
	}

	private static void loadProperties() {
		try {
			properties = new Properties();
			properties.load(PropertiesHelper.class.getClassLoader()
					.getResourceAsStream("taxCalculator.properties"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
