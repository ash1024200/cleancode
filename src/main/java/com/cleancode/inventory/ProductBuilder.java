package com.cleancode.inventory;

import java.util.Locale;

/**
 * This class is responsible for parsing a string containing product details and
 * constructing an instance of Product.
 * 
 * @author ashwinvinod
 *
 */
public class ProductBuilder {

	private static final String PRODUCT_DETAIL_DELIMITER = " ";
	private static final String IMPORT = "import";

	private String productDetail;

	public ProductBuilder(String productDetails) {
		super();
		this.productDetail = productDetails;
	}

	public Product buildProduct() {
		String[] tokenisedInput = productDetail.split(PRODUCT_DETAIL_DELIMITER);
		double preTaxCost = getPreTaxCostFromInput(tokenisedInput);
		String productDescription = getProductDescriptionFromInput(tokenisedInput);
		Product product = new Product(preTaxCost, productDetail,
				productDescription);
		return product;
	}

	private double getPreTaxCostFromInput(String[] tokenisedInput) {
		double preTaxCost = Double
				.parseDouble(tokenisedInput[tokenisedInput.length - 1]);
		return preTaxCost;
	}

	private String getProductDescriptionFromInput(String[] tokenisedInput) {
		int startIndex = 1;
		if (tokenisedInput[1].toLowerCase(Locale.ENGLISH).contains(IMPORT)) {
			startIndex++;
		}

		StringBuilder productDescription = new StringBuilder();
		int endIndex = tokenisedInput.length - 3;
		for (int index = startIndex; index <= endIndex; index++) {
			productDescription.append(tokenisedInput[index]);
			productDescription.append(PRODUCT_DETAIL_DELIMITER);
		}
		return productDescription.toString().trim();
	}

}
