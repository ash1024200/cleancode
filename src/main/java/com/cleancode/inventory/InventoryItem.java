package com.cleancode.inventory;

/**
 * Represents the physical instance of a Product along with its quantity.
 * 
 * @author ashwinvinod
 *
 */
public class InventoryItem {

	private static final String PRODUCT_DETAIL_DELIMITER = " ";
	private Product product;
	private int quantity;

	public InventoryItem(String productDetail) {
		ProductBuilder productBuilder = new ProductBuilder(productDetail);
		Product product = productBuilder.buildProduct();
		this.product = product;
		this.quantity = getQuantityFromProductDetails(productDetail);
	}

	private int getQuantityFromProductDetails(String productDetail) {
		String quantity = productDetail.split(PRODUCT_DETAIL_DELIMITER)[0];
		return Integer.parseInt(quantity);
	}

	public Product getProduct() {
		return product;
	}

	public int getQuantity() {
		return quantity;
	}

}
