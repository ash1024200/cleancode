package com.cleancode.inventory;

/**
 * Represents a standard item that can be purchased.
 * 
 * @author ashwinvinod
 *
 */
public class Product {
	private double preTaxCost;
	private String unparsedProductDetails;
	private String description;

	public Product(double preTaxCost, String unparsedProductDetails,
			String description) {
		super();
		this.preTaxCost = preTaxCost;
		this.unparsedProductDetails = unparsedProductDetails;
		this.description = description;
	}

	public double getPreTaxCost() {
		return preTaxCost;
	}

	public String getDescription() {
		return description;
	}

	public String getUnparsedProductDetails() {
		return unparsedProductDetails;
	}
}
