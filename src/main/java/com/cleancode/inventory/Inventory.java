package com.cleancode.inventory;

import java.util.ArrayList;
import java.util.List;

public class Inventory {

	private List<InventoryItem> inventoryItems = new ArrayList<InventoryItem>();

	public Inventory(List<String> productDetails) {
		super();
		for (String productDetail : productDetails) {
			InventoryItem inventoryItem = new InventoryItem(productDetail);
			inventoryItems.add(inventoryItem);
		}
	}

	public List<InventoryItem> getInventoryItems() {
		return inventoryItems;
	}

}
