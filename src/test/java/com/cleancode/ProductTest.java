package com.cleancode;

import org.junit.Assert;
import org.junit.Test;

import com.cleancode.inventory.Product;
import com.cleancode.inventory.ProductBuilder;

public class ProductTest {

	@Test
	public void testProductCreationWithBook() {
		Product product = new ProductBuilder("1 book at 12.49").buildProduct();
		Assert.assertEquals("book", product.getDescription());
		Assert.assertEquals(12.49, product.getPreTaxCost(), 0.0);
	}

	@Test
	public void testProductCreationWithMusicCD() {
		Product product = new ProductBuilder("1 music CD at 14.99")
				.buildProduct();
		Assert.assertEquals("music CD", product.getDescription());
		Assert.assertEquals(14.99, product.getPreTaxCost(), 0.0);
	}

	@Test
	public void testProductCreationWithChocolateBar() {
		Product product = new ProductBuilder("1 chocolate bar at 0.85")
				.buildProduct();
		Assert.assertEquals("chocolate bar", product.getDescription());
		Assert.assertEquals(0.85, product.getPreTaxCost(), 0.0);
	}

	@Test
	public void testProductCreationWithImportedChocolates() {
		Product product = new ProductBuilder(
				"1 imported box of chocolates at 10.00").buildProduct();
		Assert.assertEquals("box of chocolates", product.getDescription());
		Assert.assertEquals(10.0, product.getPreTaxCost(), 0.0);
	}

	@Test
	public void testProductCreationWithPerfume() {
		Product product = new ProductBuilder("1 bottle of perfume at 18.99")
				.buildProduct();
		Assert.assertEquals("bottle of perfume", product.getDescription());
		Assert.assertEquals(18.99, product.getPreTaxCost(), 0.0);
	}

	@Test
	public void testProductCreationWithImportedPerfume() {
		Product product = new ProductBuilder(
				"1 imported bottle of perfume at 47.50").buildProduct();
		Assert.assertEquals("bottle of perfume", product.getDescription());
		Assert.assertEquals(47.50, product.getPreTaxCost(), 0.0);
	}

	@Test
	public void testProductCreationWithHeadachePills() {
		Product product = new ProductBuilder(
				"1 packet of headache pills at 9.75").buildProduct();
		Assert.assertEquals("packet of headache pills",
				product.getDescription());
		Assert.assertEquals(9.75, product.getPreTaxCost(), 0.0);
	}
}
