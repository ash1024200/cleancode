package com.cleancode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.cleancode.inventory.Inventory;
import com.cleancode.receipt.Receipt;
import com.cleancode.receipt.ReceiptCalculator;

public class TaxCalculatorTest {

	@Test
	public void testInputOne() {
		List<String> inputStrings = new ArrayList<String>();
		inputStrings.add("1 book at 12.49");
		inputStrings.add("1 music CD at 14.99");
		inputStrings.add("1 chocolate bar at 0.85");
		
		Receipt receipt = buildReceipt(inputStrings);
		
		Assert.assertEquals("29.83", receipt.getTotalAmount());
		Assert.assertEquals("1.50", receipt.getTotalSalesTax());

		List<String> productDetails = receipt.getProductDetails();
		Assert.assertNotNull(productDetails);
		Assert.assertTrue(productDetails.contains("1 book: 12.49"));
		Assert.assertTrue(productDetails.contains("1 music CD: 16.49"));
		Assert.assertTrue(productDetails.contains("1 chocolate bar: 0.85"));
	}

	@Test
	public void testInputTwo() {
		List<String> inputStrings = Arrays.asList(new String[] {
				"1 imported box of chocolates at 10.00",
				"1 imported bottle of perfume at 47.50" });
		
		Receipt receipt = buildReceipt(inputStrings);
		
		Assert.assertEquals("65.15", receipt.getTotalAmount());
		Assert.assertEquals("7.65", receipt.getTotalSalesTax());

		List<String> productDetails = receipt.getProductDetails();
		Assert.assertNotNull(productDetails);
		Assert.assertTrue(productDetails
				.contains("1 imported box of chocolates: 10.50"));
		Assert.assertTrue(productDetails
				.contains("1 imported bottle of perfume: 54.65"));
	}

	@Test
	public void testInputThree() {
		List<String> inputStrings = Arrays.asList(new String[] {
				"1 imported bottle of perfume at 27.99",
				"1 bottle of perfume at 18.99",
				"1 packet of headache pills at 9.75",
				"1 box of imported chocolates at 11.25" });
		
		Receipt receipt = buildReceipt(inputStrings);

		Assert.assertEquals("74.68", receipt.getTotalAmount());
		Assert.assertEquals("6.70", receipt.getTotalSalesTax());

		List<String> productDetails = receipt.getProductDetails();
		Assert.assertNotNull(productDetails);
		Assert.assertTrue(productDetails.contains("1 bottle of perfume: 20.89"));
		Assert.assertTrue(productDetails
				.contains("1 packet of headache pills: 9.75"));
		Assert.assertTrue(productDetails
				.contains("1 box of imported chocolates: 11.85"));
	}
	
	private Receipt buildReceipt(List<String> inputStrings) {
		Inventory inventory = new Inventory(inputStrings);
		ReceiptCalculator receiptBuilder = new ReceiptCalculator(inventory);
		Receipt receipt = receiptBuilder.buildReceipt();
		return receipt;
	}
}
